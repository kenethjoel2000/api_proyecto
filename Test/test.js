var indexRouter = require('../routes/index').indextest;
var assert = require("chai").assert;

describe("Test Index API", function(){
    it("Validar Metodo Start 'Rabbit'" , function(){
        var x = indexRouter.Empezar_Rabbit();
        assert.equal(x, true);
    });
    it("Validar luego de la Conexion" , function(){
        var x = indexRouter.Posterior();
        assert.equal(x, false);
    });
    it("Validar conexion Firebase" , function(){
        var x = indexRouter.Firebase_connect();
        assert.equal(x, false);
    });
});