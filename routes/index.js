var express = require('express');
var router = express.Router();
var amqp = require('amqplib/callback_api');
var admin = require('firebase-admin');
var fs = require('fs');

//------------------Firebase--------------------------------------------------//
var permisos = JSON.parse(fs.readFileSync('./keys/permisos.json', 'utf8'));
admin.initializeApp({
    credential: admin.credential.cert(permisos),
    databaseURL: 'https://proyecto-574eb-default-rtdb.firebaseio.com'
});
var db = admin.firestore();


//------------------RabbitCould-----------------------------------------------//
var amqpConn = null;
var pubChannel = null;
var offlinePubQueue = [];

var getdata = null;
var resdata = null;

function start() {
    try {
        amqp.connect('amqps://sgisntca:sytsfABPmbtLZcvLLHvcprkMlygkGvbw@chimpanzee.rmq.cloudamqp.com/sgisntca', function (err, conn) {
            if (err) {
                console.error("[AMQP]", err.message);
                return setTimeout(start, 1000);
            }
            conn.on("error", function (err) {
                if (err.message !== "Connection closing") {
                    console.error("[AMQP] conn error", err.message);
                }
            });
            conn.on("close", function () {
                console.error("[AMQP] reconnecting");
                return setTimeout(start, 1000);
            });
            console.log("[AMQP] connected");
            amqpConn = conn;
            whenConnected();
        });
        return true;
    }catch (e){
        return false;
    }

}


function whenConnected() {
    try{
        startPublisher();
        startWorker();
        return true;
    }catch (e){
        return false;
    }

}


function startPublisher() {
    amqpConn.createConfirmChannel(function (err, ch) {
        if (closeOnErr(err))
            return;
        ch.on("error", function (err) {
            console.error("[AMQP] channel error", err.message);
        });
        ch.on("close", function () {
            console.log("[AMQP] channel closed");
        });

        pubChannel = ch;
        while (true) {
            var m = offlinePubQueue.shift();
            if (!m)
                break;
            publish(m[0], m[1], m[2]);
        }
    });
}

function publish(exchange, routingKey, content) {
    try {
        pubChannel.publish(exchange, routingKey, content, {persistent: true},
                function (err, ok) {
                    if (err) {
                        console.error("[AMQP] publish1", err);
                        offlinePubQueue.push([exchange, routingKey, content]);
                        pubChannel.connection.close();
                    }
                    console.log("[AMQP] publish Ok");
                });
    } catch (e) {
        console.error("[AMQP] publish2", e.message);
        offlinePubQueue.push([exchange, routingKey, content]);
    }
}
// A worker that acks messages only if processed succesfully
function startWorker() {
    amqpConn.createChannel(function (err, ch) {
        if (closeOnErr(err))
            return;
        ch.on("error", function (err) {
            console.error("[AMQP] channel error", err.message);
        });

        ch.on("close", function () {
            console.log("[AMQP] channel closed");
        });

        ch.prefetch(10);
        ch.assertQueue("sensores", {durable: true}, function (err, _ok) {
            if (closeOnErr(err))
                return;
            ch.consume("sensores", processMsg, {noAck: false});
            console.log("Worker is started");
        });

        function processMsg(msg) {
            work(msg, function (ok) {
                try {
                    if (ok)
                        ch.ack(msg);
                    else
                        ch.reject(msg, true);
                } catch (e) {
                    closeOnErr(e);
                }
            });
        }
    });
}

function work(msg, cb) {
    resdata = getdata;
    getdata = msg.content.toString();
    console.log("Got msg ", msg.content.toString());
    cb(true);
}

function closeOnErr(err) {
    if (!err)
        return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}

function salida(codigo, entrada) {
    var today = new Date();
    var date = today.getFullYear() + '-' + today.getMonth() + '-' + today.getDay();

    if (codigo == '200') return {
        mensaje : "Operación correcta",
        fecha : date,
        resultado : entrada
    }

    if (codigo=='500') return {
        mensaje : "Ocurrió un error",
        fecha : date,
        resultado : entrada
    };
}

start();


/* GET home page. */
router.get('/', async (req, res, next)  => {
    res.render('index');
});

router.get('/consumer', async (req, res, next)  => {
    console.log("getdata");
    console.log(getdata);
    console.log(JSON.parse(getdata));
    res.send(getdata);
});

router.post('/consumer', (req, res, next)  => {
    (async () => {  
        try {
            var aux = JSON.parse(getdata);
            var fb = await db.collection('DATAPROYECTO').doc('/' + "Sensores"+ '/');
            if (aux !== null && aux !== resdata) {
                fb.set({"Listado":aux}); 
            } else {
                console.log("esta nulo o es igual");
            }
            return res.status(200).send(salida("200", "Contacto creado correctamente"));
        } catch (error) {
            console.log(error);
            return res.status(500).json(salida("500",error));
        }
    })();
});


module.exports = router;





